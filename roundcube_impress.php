<?php
/*
 * RoundCube Impress Plugin
 *
 * Copyright (C) 2014, Michael Maier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Impress
 *
 * Adds a custom localised impress page to the login form and taskbar.
 *
 * @author Michael Maier
 * @url https://gitlab.com/takerukoushirou/roundcube-impress
 */
class roundcube_impress extends rcube_plugin
{

    // Active for all tasks (including logout), as the impress link should be
    // shown on the login form and (once logged in) taskbar.
    public $task = '.*';

    // Skip plugin during AJAX requests.
    public $noajax = true;

    const HideTaskbarSkinsConfigKey = 'impress_hide_taskbar_skins';
    const LoginFooterConfigKey = 'impress_login_footer';
    const ToplinePositionConfigKey = 'impress_topline_position';

    /** @var rcmail */
    private $rc;
    /** @var string */
    private $active_skin;
    /** @var string */
    protected $packageName;

    public
    function init()
    {
        $this->packageName = static::class;

        $this->rc = rcmail::get_instance();

        // Do not require configuration, use defaults.
        $this->load_config('config.inc.php.dist');
        $this->load_config('config.inc.php');

        $this->add_texts('localization/', false);

        $this->register_task('impress');
        $this->register_action('index', [ $this, 'do_index' ]);

        $this->add_hook('startup', [ $this, 'on_startup' ]);
        $this->add_hook('template_container', [ $this, 'on_template_container' ]);

        // Determine currently active skin. Refer to implementation of
        // local_skin_path() for characteristics of the hidden output property.
        $this->active_skin =
            array_keys($this->rc->output->skins)[0];
    }

    /**
     * Impress task index page. Renders impress as full page.
     */
    public
    function do_index()
    {
        $this->rc->output->set_pagetitle($this->gettext('page-title'));

        // Register handler for actual contents.
        $this->register_output_handlers($this->rc->output);

        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $this->rc->output->send("$this->packageName.page");
    }

    public
    function on_startup()
    {
        if (!$this->rc->output->framed) {
            // Check whether to skip taskbar button for current skin.
            $skinPattern = $this->rc->config->get(static::HideTaskbarSkinsConfigKey, '/larry/');

            if (!preg_match($skinPattern, $this->active_skin)) {
                // Add taskbar button.
                $this->add_button(
                    [
                        'command' => 'impress',
                        'class' => 'button-impress',
                        'classsel' => 'button-impress button-selected',
                        'innerclass' => 'button-inner',
                        'label' => "$this->packageName.title",
                        'type' => 'link',
                    ],
                    'taskbar'
                );
            }
        }

        $this->include_stylesheet(
            sprintf(
                '%s/impress.css',
                $this->local_skin_path()
            )
        );
    }

    /**
     * @param array $container
     * @return array
     */
    public
    function on_template_container($container)
    {
        $config = $this->rc->config;

        if ('topline' === substr($container['name'], 0, 7)) {
            $position = $config->get(static::ToplinePositionConfigKey, 'left');

            if ($position === true) {
                $position = 'left';
            }

            if ($position &&
                $container['name'] === "topline-$position") {
                // Simple link that triggers the impress action and that is
                // located in the topline.
                $renderer = $this->get_template_engine();

                $container['content'] .=
                    $renderer->parse("$this->packageName.topline-link", false, false);
            }
        }
        elseif ('loginfooter' === $container['name']) {
            // Whether an impress link is shown on the login page is configurable
            // but recommended as is it enforced by law in some countries.
            if ($config->get(static::LoginFooterConfigKey, true)) {
                // Show impress content box when requested. Embed impress such
                // that no specific tool (such as JavaScript) is necessary to
                // see the impress as this may be required by law.
                if (!empty($_GET['_impress'])) {
                    // Fake a content box, similar to the login form. Skin
                    // markup should be aligned with login form.
                    $renderer = $this->get_template_engine();

                    $container['content'] .=
                        $renderer->parse("$this->packageName.login-box", false, false);
                }
                else {
                    // Add link to show impress. Control is a simple GET
                    // variable that, if set, replaces the link with the
                    // actual impress contents.
                    $renderer = $this->get_template_engine();

                    $container['content'] .=
                        $renderer->parse("$this->packageName.login-link", false, false);
                }
            }
        }

        return $container;
    }

    /**
     * RoundCube object handler for "impresscontent". Fetches impress contents
     * from a static file.
     *
     * @return string
     */
    public
    function render_content()
    {
        $path = $this->locate_content('impress.html');

        if (empty($path)) {
            return "Impress content not found";
        }

        // Impress is static HTML with simple formatting. No templating
        // required.
        return @file_get_contents($path);
    }

    /**
     * Instantiates a temporary template rendering engine.
     *
     * @return rcmail_output_html
     */
    protected
    function get_template_engine()
    {
        $output = new rcmail_output_html();
        $this->register_output_handlers($output);

        return $output;
    }

    /**
     * Determines the path for content in the currently active language.
     *
     * @param string $name
     * Name of content to locate.
     *
     * @return string|null
     * Path to content on success, otherwise `null`.
     */
    protected
    function locate_content($name)
    {
        $language = $_SESSION['language'];

        $path = "$this->home/content/$language/$name";

        if (!file_exists($path)) {
            $path = "$this->home/content/en_US/$name";
        }

        if (!file_exists($path)) {
            return null;
        }

        return $path;
    }

    /**
     * Adds all plugin output handlers to a given output renderer.
     *
     * @param rcube_output $output
     * RoundCube output renderer to add plugin handlers to.
     */
    protected
    function register_output_handlers(rcube_output $output)
    {
        $output->add_handlers(
            [
                // Fetches the static HTML for the custom impress.
                'impresscontent' => [ $this, 'render_content' ],
            ]
        );
    }

}
