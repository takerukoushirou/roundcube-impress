# RoundCube Impress
Adds a custom localised impress page to the login form and taskbar.


## Features

- Contents can be localised.
- Link position is configurable.
- Login task impress page does not require JavaScript to work, hence is easily
  accessible as required by law in some countries.
- By default, the Classic, Larry and Elastic skins are supported.
- Easily extensible for other skins via templates for custom content elements.


## Installation
The plugin can either be installed via Composer or manually.
If the webmail server allows outgoing requests, Composer is the more comfortable option as it automatically resolves dependencies and simplifies updates.

### Using Composer
⚠️ Note that it is not recommended to run Composer as `root`.
Plugin installation requires script support enabled, as the RoundCube installer needs to move the downloaded plugin to the plugins directory and optionally can enable the plugin automatically.

1. Get [Composer][getcomposer].
1. Within the RoundCube webmail root directory, add a new dependency to the plugin.
   Use `dev-main` as the version constraint for the latest in-development version.
   ```shell
   php composer.phar require 'takerukoushirou/roundcube-impress:^1.0'
   ```
1. Composer may ask whether to enable the plugin. Confirm with `y`.

To update the installed plugin to the latest version, simply run:
```shell
php composer.phar update --no-dev
```

### Manual
1. Download the latest release archive or checkout the latest release branch.
1. Extract the contents into a folder named `roundcube_impress` within the `plugins` directory of your RoundCube installation.

There are no external dependencies.

Repeat manual installation for updates.
It may be advisable to keep multiple versions of the plugin in separate folders and use a symlink named `roundcube_impress` from within the `plugins` directory to the latest version folder instead.


## Configuration

To enable the plugin, add `roundcube_impress` to the `$config['plugins']` array in the RoundCube configuration file.  
When using Composer, the installation routine will ask whether to automatically enable the plugin.

### Options
Configuration of the plugin itself is optional, as it will automatically load the defaults from `config.inc.php.dist`.

To enable customisation, first navigate to the plugin directory and copy `config.inc.php.dist` to `config.inc.php` on a fresh installation.  
When using Composer, a copy is automatically created.

Edit `config.inc.php` within the plugin directory as needed.  
All options and their accepted values are described in `config.inc.php.dist`.

### Contents
Modify the `content/en_US/impress.html` default impress as needed.
This is also used for every other localisation by default.  
Remove the example localisation for `de_DE` if not used.

To add support for other locales, copy the `en_US` folder within the `content` directory and name the new folder according to the new locale. Modify the contained files as needed. 


## License
![GNU General Public License v3 logo][gpl-license-logo]  
[GNU General Public License v3][gpl-license] or higher.
See [LICENSE](LICENSE) file for details.

> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.


[getcomposer]: https://getcomposer.org/download/
[gpl-license]: https://www.gnu.org/licenses/gpl-3.0.en.html
[gpl-license-logo]: https://www.gnu.org/graphics/gplv3-88x31.png